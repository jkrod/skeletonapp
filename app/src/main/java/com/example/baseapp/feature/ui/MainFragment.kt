package com.example.baseapp.feature.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.baseapp.MainApplication
import com.example.baseapp.R
import com.example.baseapp.databinding.MainFragmentBinding
import kotlinx.coroutines.InternalCoroutinesApi
import javax.inject.Inject

class MainFragment : Fragment() {

    @Inject
    internal lateinit var presenter: MainPresenter

    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.btnNavigate.setOnClickListener {
            findNavController()
                .navigate(R.id.action_mainFragment_to_otherFragment)
        }
        return binding.root
    }

    override fun onAttach(context: Context) {
        (context.applicationContext as MainApplication)
            .appComponent
            .inject(this)
        super.onAttach(context)
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreate()

        // Listen for updates
        presenter.liveData.observe(viewLifecycleOwner, Observer { value ->
            binding.textResult.text = value
        })
    }

//    override fun onDestroyView() {
//        super.onDestroyView()
////        presenter.destroy()
//    }
}
