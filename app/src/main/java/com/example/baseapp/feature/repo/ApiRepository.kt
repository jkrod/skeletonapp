package com.example.baseapp.feature.repo

import com.example.baseapp.db.DbDao
import com.example.baseapp.db.DbModel
import com.example.networking.feature.ApiService
import com.example.networking.feature.NetworkModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ApiRepository @Inject constructor(
    private val service: ApiService,
    private val dbDao: DbDao
) {
    suspend fun getValue(id: String): NetworkModel {
        val value = service.getValue(id)
        // Store value in DB when succeeded
        dbDao.insertModel(
            DbModel(
                id = value.id,
                title = value.title
            )
        )
        return value
    }

    suspend fun getCachedValue(id: String): DbModel? {
        return dbDao.getDbModel(id)
    }

    fun observeAllValues(): Flow<List<DbModel>> {
        return dbDao.getAll()
    }

    suspend fun clearAll() {
        dbDao.clear()
    }
}