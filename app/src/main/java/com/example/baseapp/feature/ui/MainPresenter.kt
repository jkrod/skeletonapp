package com.example.baseapp.feature.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.baseapp.feature.repo.ApiRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainPresenter @Inject constructor(private val repository: ApiRepository) :
    CoroutineScope by CoroutineScope(Dispatchers.Main) {

    private val _liveData: MutableLiveData<String> = MutableLiveData()

    // Stream for any observer to get any responses from
    internal val liveData: LiveData<String> = _liveData

    private fun fetchInfo() {
        launch {
            val result = runCatching {
                repository.getValue("1")
                delay(1000)
                repository.getValue("2")
                delay(1000)
                repository.getValue("3")
            }

            Log.d("TAG", result.toString())
        }
    }

    @InternalCoroutinesApi
    internal fun onCreate() {
        launch {
            repository.clearAll()
            val response = repository.getValue("6")
            Log.d("TAG", response.toString())
            repository.observeAllValues()
                .collect { value ->
                    _liveData.value =
                        value.joinToString(separator = "\n") { dbModel ->
                            "${dbModel.id}:${dbModel.title}"
                        }
                }
        }

        fetchInfo()
    }

//    internal fun destroy() {
//        cancel()
//    }
}