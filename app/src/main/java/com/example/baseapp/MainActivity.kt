package com.example.baseapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.example.baseapp.feature.ui.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

    override fun onNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment)
            .navigateUp()
    }
}
