package com.example.baseapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface DbDao {
    @Query("SELECT * FROM DbModel ORDER BY id")
    fun getAll(): Flow<List<DbModel>>

    @Query("SELECT * FROM DbModel WHERE id=(:id)")
    suspend fun getDbModel(id: String): DbModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertModel(dbModel: DbModel)

    @Query("DELETE FROM DbModel")
    suspend fun clear()
}