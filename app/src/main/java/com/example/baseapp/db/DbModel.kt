package com.example.baseapp.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DbModel(
    @PrimaryKey
    val id: String,
    val title: String
)
