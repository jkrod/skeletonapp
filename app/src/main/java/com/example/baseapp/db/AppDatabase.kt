package com.example.baseapp.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DbModel::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dbDao(): DbDao
}
