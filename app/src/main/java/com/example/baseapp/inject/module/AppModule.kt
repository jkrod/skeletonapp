package com.example.baseapp.inject.module

import com.example.baseapp.MainApplication
import com.example.baseapp.inject.qualifier.IoScheduler
import com.example.baseapp.inject.qualifier.UiScheduler
import com.example.baseapp.inject.scope.AppScope
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
class AppModule(private val application: MainApplication) {
    @Provides
    @AppScope
    fun applicationContext(): MainApplication {
        return application
    }

    @AppScope
    @Provides
    @IoScheduler
    fun ioScheduler(): CoroutineDispatcher = Dispatchers.IO

    @AppScope
    @Provides
    @UiScheduler
    fun uiScheduler(): CoroutineDispatcher = Dispatchers.Main

}