package com.example.baseapp.inject.module

import androidx.room.Room
import com.example.baseapp.MainApplication
import com.example.baseapp.db.AppDatabase
import com.example.baseapp.db.DbDao
import com.example.baseapp.inject.scope.AppScope
import dagger.Module
import dagger.Provides


private const val DATABASE_NAME = "app-database"

@Module
object DbModule {

    @AppScope
    @Provides
    fun appDatabase(context: MainApplication): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
            // Doing destructive migrations for time being, would definitely not want this for production
            .fallbackToDestructiveMigration()
            .build()
    }

    @AppScope
    @Provides
    fun dbDao(appDb: AppDatabase): DbDao {
        return appDb.dbDao()
    }
}
