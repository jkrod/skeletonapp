package com.example.baseapp.inject.component

import com.example.baseapp.feature.ui.MainFragment
import com.example.baseapp.inject.module.AppModule
import com.example.baseapp.inject.module.DbModule
import com.example.baseapp.inject.module.NetworkModule
import com.example.baseapp.inject.scope.AppScope
import dagger.Component


@AppScope
@Component(
    modules = [
        NetworkModule::class,
        DbModule::class,
        AppModule::class
    ]
)
interface AppComponent {
    fun inject(mainFragment: MainFragment)
}
