package com.example.baseapp.inject.module

import com.example.baseapp.BuildConfig
import com.example.baseapp.inject.qualifier.ApiEndpoint
import com.example.baseapp.inject.scope.AppScope
import com.example.networking.feature.ApiService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
object NetworkModule {

    @Provides
    @AppScope
    fun moshi(): Moshi {
        return Moshi.Builder()
            // Using Kotlin adapter factory to ensure null safety
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    @Provides
    @AppScope
    fun httpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()

        // Only add logging for debug builds
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(
                HttpLoggingInterceptor()
                    .apply {
                        level = HttpLoggingInterceptor.Level.BASIC
                    }
            )
        }

        return builder.build()
    }

    @Provides
    @AppScope
    fun retrofitInstance(
        moshi: Moshi,
        @ApiEndpoint apiEndpoint: String,
        httpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(apiEndpoint)
            .client(httpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    @AppScope
    @ApiEndpoint
    fun apiEndpoint(): String = "https://jsonplaceholder.typicode.com"

    @Provides
    @AppScope
    fun apiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}