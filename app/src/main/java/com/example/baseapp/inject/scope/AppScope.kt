package com.example.baseapp.inject.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class AppScope