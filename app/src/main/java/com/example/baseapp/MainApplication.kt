package com.example.baseapp

import android.app.Application
import com.example.baseapp.inject.component.AppComponent
import com.example.baseapp.inject.component.DaggerAppComponent
import com.example.baseapp.inject.module.AppModule
import com.example.baseapp.inject.module.NetworkModule

class MainApplication : Application() {

    val appComponent: AppComponent by lazy(LazyThreadSafetyMode.NONE) { setAppComponent() }

    private fun setAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

}