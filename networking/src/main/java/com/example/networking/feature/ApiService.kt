package com.example.networking.feature

import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("todos/{id}")
    suspend fun getValue(@Path("id") id: String): NetworkModel
}