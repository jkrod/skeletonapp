package com.example.networking.feature

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NetworkModel(
    val id: String,
    val title: String
)
